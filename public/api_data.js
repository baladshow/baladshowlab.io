define({ "api": [
  {
    "type": "post",
    "url": "/auth/activation-code",
    "title": "Send Activation Code",
    "group": "Auth",
    "name": "Activation_Code",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phoneNumber",
            "description": "<p>phoneNumber</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phoneNumber",
            "description": "<p>phoneNumber</p>"
          }
        ]
      }
    },
    "version": "1.0.0",
    "filename": "app/controllers/AuthController.ts",
    "groupTitle": "Auth",
    "sampleRequest": [
      {
        "url": "https://baladshow.com/api/v1/auth/activation-code"
      }
    ]
  },
  {
    "type": "post",
    "url": "/auth/token",
    "title": "Get Token",
    "group": "Auth",
    "name": "Get_Token",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phoneNumber",
            "description": "<p>phone number.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT-Token.</p>"
          }
        ]
      }
    },
    "version": "1.0.0",
    "filename": "app/controllers/AuthController.ts",
    "groupTitle": "Auth",
    "sampleRequest": [
      {
        "url": "https://baladshow.com/api/v1/auth/token"
      }
    ]
  },
  {
    "type": "post",
    "url": "/auth/google/login",
    "title": "login with google",
    "group": "Auth",
    "name": "Login",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tokenId",
            "description": "<p>tokenId (from google auth).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT-Token.</p>"
          }
        ]
      }
    },
    "version": "1.0.0",
    "filename": "app/controllers/AuthController.ts",
    "groupTitle": "Auth",
    "sampleRequest": [
      {
        "url": "https://baladshow.com/api/v1/auth/google/login"
      }
    ]
  },
  {
    "type": "put",
    "url": "/auth/refresh-token",
    "title": "Refresh Token",
    "group": "Auth",
    "name": "Refresh_Token",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>token.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>token.</p>"
          }
        ]
      }
    },
    "description": "<p>generate a new token from expired one.</p>",
    "version": "1.0.0",
    "filename": "app/controllers/AuthController.ts",
    "groupTitle": "Auth",
    "sampleRequest": [
      {
        "url": "https://baladshow.com/api/v1/auth/refresh-token"
      }
    ]
  },
  {
    "type": "post",
    "url": "/auth/google/signup",
    "title": "signup with google",
    "group": "Auth",
    "name": "SignUp",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tokenId",
            "description": "<p>tokenId (from google auth).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phoneNumber",
            "description": "<p>phoneNumber.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>sms code.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT-Token.</p>"
          }
        ]
      }
    },
    "version": "1.0.0",
    "filename": "app/controllers/AuthController.ts",
    "groupTitle": "Auth",
    "sampleRequest": [
      {
        "url": "https://baladshow.com/api/v1/auth/google/signup"
      }
    ]
  },
  {
    "type": "post",
    "url": "/auth/signup",
    "title": "Signup",
    "group": "Auth",
    "name": "Signup",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phoneNumber",
            "description": "<p>PhoneNumber.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>verification sms code.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT-Token.</p>"
          }
        ]
      }
    },
    "description": "<p>token will be sent after signup</p>",
    "version": "1.0.0",
    "filename": "app/controllers/AuthController.ts",
    "groupTitle": "Auth",
    "sampleRequest": [
      {
        "url": "https://baladshow.com/api/v1/auth/signup"
      }
    ]
  },
  {
    "type": "post",
    "url": "/auth/verify-phone",
    "title": "Verify PhoneNumber",
    "group": "Auth",
    "name": "Verify_Phone_Number",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>sms-code.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phoneNumber",
            "description": "<p>phoneNumber.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_Token.</p>"
          }
        ]
      }
    },
    "version": "1.0.0",
    "filename": "app/controllers/AuthController.ts",
    "groupTitle": "Auth",
    "sampleRequest": [
      {
        "url": "https://baladshow.com/api/v1/auth/verify-phone"
      }
    ]
  },
  {
    "type": "post",
    "url": "/auth/is-unique",
    "title": "check uniqueness of a field",
    "group": "Auth",
    "name": "check_uniqueness",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "field",
            "description": "<p>field (email or phoneNumber).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "value",
            "description": "<p>value.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "unique",
            "description": "<p>is unique</p>"
          }
        ]
      }
    },
    "version": "1.0.0",
    "filename": "app/controllers/AuthController.ts",
    "groupTitle": "Auth",
    "sampleRequest": [
      {
        "url": "https://baladshow.com/api/v1/auth/is-unique"
      }
    ]
  },
  {
    "type": "get",
    "url": "/set",
    "title": "Get Sets",
    "group": "Set",
    "name": "Get_Sets",
    "description": "<p>Acceptable query string parameters: <code>loaded</code></p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "sets",
            "description": "<p>List of Sets</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "sets.id",
            "description": "<p>Id</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "sets.number",
            "description": "<p>Number Of collections</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "sets.query",
            "description": "<p>query for searching collections</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "sets.title",
            "description": "<p>Title</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "sets.icon",
            "description": "<p>Icon Title</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "sets.isFeatured",
            "description": "<p>is Featured</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "sets.videoCollections",
            "description": "<p>Video Collections (loaded=true)</p>"
          },
          {
            "group": "Success 200",
            "type": "String[]",
            "optional": false,
            "field": "sets.types",
            "description": "<p>Device Types ex: ['MOBILE','DESKTOP']</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "sets.createdAt",
            "description": "<p>Creation Date</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "sets.updatedAt",
            "description": "<p>Update Date</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "https://baladshow.com/api/v1/sets?loaded=true"
      }
    ],
    "version": "1.0.0",
    "filename": "app/controllers/SetController.ts",
    "groupTitle": "Set"
  },
  {
    "type": "get",
    "url": "/taxonSet",
    "title": "Get TaxonSets",
    "group": "TaxonSet",
    "name": "Get_TaxonSets",
    "description": "<p>Acceptable query string parameters: <code>loaded</code></p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "taxonSets",
            "description": "<p>List of TaxonSets</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "taxonSets.id",
            "description": "<p>Id</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "taxonSets.number",
            "description": "<p>Number Of collections</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "taxonSets.query",
            "description": "<p>query for searching collections</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "taxonSets.title",
            "description": "<p>Title</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "taxonSets.icon",
            "description": "<p>Icon Title</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "taxonSets.isFeatured",
            "description": "<p>is Featured</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "taxonSets.videoCollections",
            "description": "<p>Video Collections (loaded=true)</p>"
          },
          {
            "group": "Success 200",
            "type": "String[]",
            "optional": false,
            "field": "taxonSets.types",
            "description": "<p>Device Types ex: ['MOBILE','DESKTOP']</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "taxonSets.createdAt",
            "description": "<p>Creation Date</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "taxonSets.updatedAt",
            "description": "<p>Update Date</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "https://baladshow.com/api/v1/taxonSets?loaded=true"
      }
    ],
    "version": "1.0.0",
    "filename": "app/controllers/TaxonSetController.ts",
    "groupTitle": "TaxonSet"
  },
  {
    "type": "get",
    "url": "/taxon/:id",
    "title": "Get Taxon",
    "group": "Taxonomy",
    "name": "Get_Taxon_by_id",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "slug",
            "description": "<p>Slug</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "position",
            "description": "<p>Position</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "level",
            "description": "<p>Level</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "color",
            "description": "<p>Color</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Title</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "lineage",
            "description": "<p>Lineage</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "parentId",
            "description": "<p>Parent Id</p>"
          },
          {
            "group": "Success 200",
            "type": "String[]",
            "optional": false,
            "field": "keywords",
            "description": "<p>Keywords</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "createdAt",
            "description": "<p>Creation Date</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updatedAt",
            "description": "<p>Update Date</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "https://baladshow.com/api/v1/taxon/2"
      }
    ],
    "version": "1.0.0",
    "filename": "app/controllers/TaxonController.ts",
    "groupTitle": "Taxonomy"
  },
  {
    "type": "get",
    "url": "/taxon",
    "title": "Get Taxons",
    "group": "Taxonomy",
    "name": "Get_Taxons",
    "description": "<p>Acceptable query string parameters: <code>parentId</code>, <code>level</code>, <code>levels</code></p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "taxons",
            "description": "<p>List of Taxons</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "taxons.id",
            "description": "<p>Id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "taxons.slug",
            "description": "<p>Slug</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "taxons.position",
            "description": "<p>Position</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "taxons.level",
            "description": "<p>Level</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "taxons.color",
            "description": "<p>Color</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "taxons.title",
            "description": "<p>Title</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "taxons.lineage",
            "description": "<p>Lineage</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "taxons.parentId",
            "description": "<p>Parent Id</p>"
          },
          {
            "group": "Success 200",
            "type": "String[]",
            "optional": false,
            "field": "taxons.keywords",
            "description": "<p>Keywords</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "taxons.createdAt",
            "description": "<p>Creation Date</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "taxons.updatedAt",
            "description": "<p>Update Date</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "https://baladshow.com/api/v1/taxon?levels=1,2"
      }
    ],
    "version": "1.0.0",
    "filename": "app/controllers/TaxonController.ts",
    "groupTitle": "Taxonomy"
  },
  {
    "type": "put",
    "url": "/user/:id",
    "title": "Update User",
    "group": "User",
    "name": "Update_User",
    "description": "<p>Authorization token needed.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "email",
            "description": "<p>Email.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "name",
            "description": "<p>Full Name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "biography",
            "description": "<p>Biography.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object[]",
            "optional": true,
            "field": "picture",
            "description": "<p>Picture.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "picture.src",
            "description": "<p>Base64 Picture ex:'data:image/jpeg;base64,...'.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"name\": \"John Doe\",\n  \"picture\":{\n     \"src\":\"data:image/jpeg;base64,...\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "phoneNumber",
            "description": "<p>Phone Number</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "biography",
            "description": "<p>Biography</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name</p>"
          }
        ]
      }
    },
    "version": "1.0.0",
    "filename": "app/controllers/UserController.ts",
    "groupTitle": "User"
  },
  {
    "type": "Get",
    "url": "/video/:id/manifest.mpd",
    "title": "Get manifest file",
    "group": "Video",
    "name": "Get_Manifest_file",
    "sampleRequest": [
      {
        "url": "https://baladshow.com/api/v1/video/1/manifest.mpd"
      }
    ],
    "version": "1.0.0",
    "filename": "app/controllers/VideoController.ts",
    "groupTitle": "Video"
  },
  {
    "type": "get",
    "url": "/video",
    "title": "Get Videos",
    "group": "Video",
    "name": "Get_Videos",
    "description": "<p>Acceptable query string parameters: <code>videoCollectionId</code>, <code>slug</code></p> <p>ex: /video/?videoCollectionId=2</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "videos",
            "description": "<p>List of Videos</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "videos.id",
            "description": "<p>Id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "videos.slug",
            "description": "<p>Slug</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "videos.title",
            "description": "<p>Title</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "videos.description",
            "description": "<p>Description</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "videos.duration",
            "description": "<p>Duration</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "videos.isPublished",
            "description": "<p>is Published</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "videos.isFree",
            "description": "<p>is Free</p>"
          },
          {
            "group": "Success 200",
            "type": "Dub[]",
            "optional": false,
            "field": "videos.dubs",
            "description": "<p>Dubs (not yet)</p>"
          },
          {
            "group": "Success 200",
            "type": "Dubbing[]",
            "optional": false,
            "field": "videos.dubbings",
            "description": "<p>Dubbings</p>"
          },
          {
            "group": "Success 200",
            "type": "Translation[]",
            "optional": false,
            "field": "videos.translations",
            "description": "<p>Translations</p>"
          },
          {
            "group": "Success 200",
            "type": "Subtitle[]",
            "optional": false,
            "field": "videos.subtitles",
            "description": "<p>Subtitles</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "videos.createdAt",
            "description": "<p>Creation Date</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "videos.updatedAt",
            "description": "<p>Update Date</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "https://baladshow.com/api/v1/video?_attributes=id,slug"
      }
    ],
    "version": "1.0.0",
    "filename": "app/controllers/VideoController.ts",
    "groupTitle": "Video"
  },
  {
    "type": "get",
    "url": "/videoCollection/:id",
    "title": "Get Video Collection",
    "group": "Video_Collection",
    "name": "Get_Video_Collection_by_id",
    "description": "<p>Acceptable Relation Attributes: <code>reviews</code>, <code>poster</code></p> <p>ex: /videoCollection/1?_attributes=poster</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Id</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "rate",
            "description": "<p>Rate</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "price",
            "description": "<p>Price</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ratesNo",
            "description": "<p>Number of Rates</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "trendFactor",
            "description": "<p>Trend Factor</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "mainTaxonId",
            "description": "<p>Main Taxon Id</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "companyId",
            "description": "<p>Company Id</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "posterId",
            "description": "<p>Poster Id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "slug",
            "description": "<p>Slug</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Title</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Description</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "duration",
            "description": "<p>Duration</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "isPublished",
            "description": "<p>is Published</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "isFeatured",
            "description": "<p>is Featured</p>"
          },
          {
            "group": "Success 200",
            "type": "String[]",
            "optional": false,
            "field": "keywords",
            "description": "<p>Keywords</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "createdAt",
            "description": "<p>Creation Date</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updatedAt",
            "description": "<p>Update Date</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "https://baladshow.com/api/v1/videoCollection/1?_attributes=poster,rate"
      }
    ],
    "version": "1.0.0",
    "filename": "app/controllers/VideoCollectionController.ts",
    "groupTitle": "Video_Collection"
  },
  {
    "type": "get",
    "url": "/videoCollection",
    "title": "Get Video Collections",
    "group": "Video_Collection",
    "name": "Get_Video_Collections",
    "description": "<p>Acceptable query string parameters: <code>taxonIds</code>, <code>companyId</code>, <code>slug</code>, <code>q</code></p> <p>ex: /videoCollection/1?taxonIds=2,3&amp;q=art</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "videoCollections",
            "description": "<p>List of Video Collections</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "videoCollections.id",
            "description": "<p>Id</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "videoCollections.rate",
            "description": "<p>Rate</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "videoCollections.price",
            "description": "<p>Price</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "videoCollections.ratesNo",
            "description": "<p>Number of Rates</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "videoCollections.trendFactor",
            "description": "<p>Trend Factor</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "videoCollections.mainTaxonId",
            "description": "<p>Main Taxon Id</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "videoCollections.companyId",
            "description": "<p>Company Id</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "videoCollections.posterId",
            "description": "<p>Poster Id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "videoCollections.slug",
            "description": "<p>Slug</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "videoCollections.title",
            "description": "<p>Title</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "videoCollections.description",
            "description": "<p>Description</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "videoCollections.duration",
            "description": "<p>Duration</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "videoCollections.isPublished",
            "description": "<p>is Published</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "videoCollections.isFeatured",
            "description": "<p>is Featured</p>"
          },
          {
            "group": "Success 200",
            "type": "String[]",
            "optional": false,
            "field": "videoCollections.keywords",
            "description": "<p>Keywords</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "videoCollections.createdAt",
            "description": "<p>Creation Date</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "videoCollections.updatedAt",
            "description": "<p>Update Date</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "https://baladshow.com/api/v1/videoCollection?_attributes=poster,rate"
      }
    ],
    "version": "1.0.0",
    "filename": "app/controllers/VideoCollectionController.ts",
    "groupTitle": "Video_Collection"
  }
] });
